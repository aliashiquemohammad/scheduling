<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <!-- Custom fonts for this template -->
    <link href="{{asset( 'public/css/bootstrap.min.css' )}}" rel="stylesheet">
    <link href="{{asset( 'public/css/app.css' )}}" rel="stylesheet">
    <link href="{{asset( 'public/css/all.css' )}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{asset( 'public/css/_grayscale.min.css' )}}" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>  
<body id="page-top">

    <!-- Navigation -->
    @include( 'includes.nav' )

    <!-- Header -->
    
    @yield( 'content' )

    <!-- Footer -->
    <footer class="bg-black small text-center text-white-50" style="margin-top:20px;">
      <div class="container">
        Copyright &copy; Your Website 2018
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="{{asset( 'public/js/jquery.min.js' )}}"></script>
    <script src="{{asset( 'public/js/bootstrap.js' )}}"></script>

    <!-- Plugin JavaScript -->
    <script src="{{asset( 'public/js/jquery.easing.min.js' )}}"></script>

    <!-- Custom scripts for this template -->
    <script src="{{asset( 'public/js/grayscale.min.js' )}}"></script>

  </body>
</body>
</html>

