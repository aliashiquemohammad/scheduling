<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('tenant_id');
            $table->foreign( 'user_id' )->references('id')->on('users');
            $table->foreign( 'tenant_id' )->references('id')->on('tenants');
            $table->string( 'title' );
            $table->string( 'description' );
            $table->date( 'start_date' );
            $table->date( 'end_date' );
            $table->integer( 'duration' );
            $table->integer( 'max_appointments' );
            $table->string( 'price' );
            $table->string( 'status' );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
