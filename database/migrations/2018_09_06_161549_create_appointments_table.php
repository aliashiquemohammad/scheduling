<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->increments('id');
            $table->date('schedule_date');
            $table->time('schedule_time');
            $table->string('payment_status');
            $table->string('appointment_status');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('event_id');
            $table->foreign( 'user_id' )->references('id')->on('users');
            $table->foreign( 'event_id' )->references('id')->on('events');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
