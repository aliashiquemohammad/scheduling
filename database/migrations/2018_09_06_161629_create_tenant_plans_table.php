<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenantPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subscription_status');
            $table->date('subscription_start');
            $table->date('subscription_end');
            $table->unsignedInteger('tenant_id');
            $table->unsignedInteger('plan_id');
            $table->foreign( 'tenant_id' )->references('id')->on('tenants');
            $table->foreign( 'plan_id' )->references('id')->on('plans');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_plans');
    }
}
