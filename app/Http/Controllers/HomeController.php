<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function events(){
        return view( 'pages.events' );
    }

    public function dashboard(){
        $user_id = auth()->user()->full_name;
        var_dump( $user_id );
        $data = [ 'user_data' => $user_id ];
        return view( 'pages.dashboard', $data );
    }
}


